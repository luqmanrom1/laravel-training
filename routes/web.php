<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/{name}', 'UserController@getUser');

Route::get('/users/{name}/age/{age}', 'UserController@getUserAge');


Route::get('/query', 'TaskController@testQuery');





Route::group(['middleware' => 'auth:web'], function() {
    Route::get('/tasks/create', 'TaskController@getCreateForm');

    Route::post('/tasks/create', 'TaskController@postCreateTask')
        ->name('create.tasks');

    Route::post('/tasks/edit/{id}', 'TaskController@putEditTask')->name('edit.tasks');

    Route::post('tasks/delete/{id}', 'TaskController@postDeleteTask')->name('delete.tasks');

    Route::get('/tasks/index', 'TaskController@getTaskList');

    Route::get('/tasks/{id}', 'TaskController@getTaskDetails');

    Route::get('/profiles/user', 'ProfileController@getProfileForm');

    Route::post('/profiles/{user_id}', 'ProfileController@postProfileForm')->name('profiles.edit');


});
