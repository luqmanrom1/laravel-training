<?php

use Illuminate\Http\Request;


Route::group(['middleware' => 'auth:api'], function() {

    Route::get('/user', 'UserController@getApiUser');

    Route::get('/tasks', 'TaskController@getApiTasks');

    Route::post('/tasks', 'TaskController@postApiTasks');

});


Route::post('/login', 'UserController@postLoginAPI');






