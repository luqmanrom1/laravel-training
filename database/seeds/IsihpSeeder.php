<?php

use Illuminate\Database\Seeder;

class IsihpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Create will save to database

        factory(App\User::class, 2)->create()->each(function ($user) {

            // Will return collection. Not saved to database yet
            $tasks = factory(\App\Task::class, 50)->make();

            // For each tasks, attach it to user and save to database
            foreach ($tasks as $task) {
                $user->task()->save($task);
            }
        });
    }
}
