<?php

use Faker\Generator as Faker;

$factory->define(\App\Profile::class, function (Faker $faker) {
    return [
        'dob' => $faker->dateTimeBetween('-30 days', '+30 days'),
        'tel' => $faker->phoneNumber
    ];
});
