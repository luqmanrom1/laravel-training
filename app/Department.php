<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    public function user() {
        return $this->hasMany(User::class);
    }

    public function task() {
        return $this->hasManyThrough(Task::class, User::class);
    }
}
