<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function getProfileForm() {

        $user = \Auth::user();

        // resources/views/profiles/details.blade.php
        return view('profiles/details')->with(['user' => $user]);

    }


    public function postProfileForm() {

        // Get current logged in user
        $user = \Auth::user();

        // Get profile of the user. Can return null if no profile exists
        $profile = $user->profile;

        // User have no profile
        if (is_null($profile)) {
            $profile = new Profile();

            // Attach profile to the user
            $profile->user_id = $user->id;

        }

        $profile->dob = request()->get('dob');

        $profile->tel = request()->get('tel');

        $profile->save();

        \Session::flash('msg', 'Changes Saved.' );

        return redirect('/profiles/user');

    }
}