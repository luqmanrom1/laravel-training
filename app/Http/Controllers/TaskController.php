<?php

namespace App\Http\Controllers;

use App\Task;

use App\User;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function getCreateForm() {

        return view('tasks.create');
    }


    public function testQuery() {


        // Query after join table with tasks
        $userTitleMakan = User::whereHas('task', function($query) {
            $query->where('title', 'Makan');
        })->get();


        $userRoleBM = User::whereHas('roles', function($query) {
           $query->where('slug', 'bahagian_maklumat');
        })
            ->with('task')
            ->get()
            ->toArray();

        // AND WHERE
        $userAndCondition = \App\User
            ::where('name', 'Luqman')
            ->where('email', 'luqmanrom@gmail.com')
            ->get()
            ->toArray();


        // OR WHERE
        $userOrCondition = \App\User
            ::where('name', 'Luqman')
            ->orWhere('email', 'dayah@dosm.gov.my')
            ->get();


        dd($userTitleMakan, $userRoleBM, $userAndCondition, $userOrCondition);
    }

    public function postCreateTask() {

        $rules = [
          'title' => 'required|min:5',
          'description' => 'required|min:10'
        ];

        $customMessages = [
            'required' => 'Isi lah :attribute',
            'min' => "Pendek sangatlah :attribute tu"
        ];

        $data = $this->validate(request(), $rules, $customMessages);

        $user = \Auth::user();

        $task = Task::create([
            'title' => request()->get('title'),
            'description' => request()->get('description'),
            'user_id' => $user->id
        ]);

        return redirect('/tasks/index');
    }

    public function getTaskList() {

        $user = \Auth::user();

        $tasks = Task
            ::where('user_id', $user->id)
//            ->orderBy('created)at', 'asc')
            ->orderBy('created_at', 'desc')
            ->paginate(10);

//        $tasks = Task::where('user_id', $user->id)->get();


        return view('tasks.list')->with(['tasks' => $tasks]);
    }


    public function getApiTasks() {

        $user = \Auth::user();

        $tasks = Task
            ::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return response($tasks, 200);
    }

    public function getTaskDetails($id) {

        $task = Task::find($id);

        return view('tasks.details')->with(['task' => $task]);

    }

    public function postApiTasks() {

        $user = \Auth::user();

        $task = Task::create([
            'title' => request()->get('title'),
            'description' => request()->get('description'),
            'user_id' => $user->id
        ]);

        return response($task, 200);
    }

    public function putEditTask($id) {

        $task = Task::find($id);

        $task->title = request()->get('title');

        $task->description = request()->get('description');

        $task->save();


        return redirect('/tasks/index');

    }

    public function postDeleteTask($id) {

        $task = Task::find($id);

        $task->delete();

        return redirect('/tasks/index');


    }

}
