<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function getUser($name)
    {

        $students = [
            'Noraziha', 'Hazirah', 'Amirah', 'Hidayah', 'Hayati', 'Sabrina', 'Jahlawati'
        ];

        return
            view('user.index')
                ->with(['name' => $name, 'occupation' => 'Programmer', 'students' => $students]);
    }

    public function getUserAge($name, $age)
    {
        return "Hello $name Your age is $age";
    }

    public function postLoginAPI() {

        $email = request()->get('email');

        $password = request()->get('password');

        $data = [];

        if (\Auth::attempt(['email' => $email, 'password' => $password])) {
            $status = 200;

            $user = \Auth::user();

            $data['user'] = $user;

            $success = true;
        } else {
            $status = 403;

            $success = false;

        }

        $data['success'] = $success;

        return response($data, $status);
    }


    public function getApiUser() {
        $user = \Auth::user();

        return response($user, 200);

    }



}