@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12 mb-3">
                <a class="btn btn-primary" href="/tasks/create">Create</a>
            </div>

            <div class="col-md-12">
                <table class="table table-striped ">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{$task->id}}</td>
                                <td>{{$task->title}}</td>
                                <td>
                                    <a href="/tasks/{{$task->id}}">
                                        Details
                                    </a>

                                    <a href="#"
                                       onclick="event.preventDefault();document.getElementById('delete-form-{{$task->id}}').submit();">
                                        Delete
                                    </a>

                                    <form id="delete-form-{{$task->id}}"
                                          action="{{ route('delete.tasks', ['id' => $task->id]) }}"
                                          method="POST"
                                          style="display: none;">
                                        {{csrf_field()}}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        {{ $tasks->links() }}
    </div>
@endsection