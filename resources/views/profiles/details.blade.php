@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(Session::has('msg'))
                    <div class="alert alert-info">
                        <a class="close" data-dismiss="alert">×</a>
                        <strong>Heads Up!</strong> {!!Session::get('msg')!!}
                    </div>
                @endif
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">User Profile</div>

                    <div class="card-body">
                        <form method="POST" action="/profiles/{{$user->id}}">
                            @csrf

                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">Date of birth</label>

                                <div class="col-md-6">
                                    <input id="title" type="date" class="form-control" name="dob" value="{{!empty($user->profile)? $user->profile->dob: ''}}" required autofocus>


                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Phone number</label>

                                <div class="col-md-6">
                                    <input type="tel" id="phone" class="form-control" name="tel" value="{{!empty($user->profile)? $user->profile->tel: ''}}">


                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
